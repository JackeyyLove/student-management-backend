package com.example.backend.logging;


import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    private final HttpServletRequest request;

    public LoggingAspect(HttpServletRequest request) {
        this.request = request;
    }

    @Before("execution(* com.example.backend.controller.StudentController.getAllStudent(..))")
    public void logRequest(JoinPoint joinPoint) {
        String method = request.getMethod();
        String path = request.getRequestURI();
        System.out.println("HTTP Method: " + method + " Request Path: " + path);
    }

    @AfterReturning(pointcut = "execution(*  com.example.backend.controller.StudentController.getAllStudent(..))", returning = "result")
    public void logResponse(JoinPoint joinPoint, Object result) {
        if (result instanceof ResponseEntity) {
            int statusCode = ((ResponseEntity<?>) result).getStatusCodeValue();
            System.out.println("Response Code: " + statusCode);
        }
    }
    @AfterReturning(pointcut = "execution(*  com.example.backend.controller.StudentController.createStudent(..))", returning = "result")
    public void logResponse1(JoinPoint joinPoint, Object result) {
        if (result instanceof ResponseEntity) {
            int statusCode = ((ResponseEntity<?>) result).getStatusCodeValue();
            System.out.println("Response Code: " + statusCode);
        }
    }
    @AfterReturning(pointcut = "execution(*  com.example.backend.controller.StudentController.deleteStudent(..))", returning = "result")
    public void logResponse2(JoinPoint joinPoint, Object result) {
        if (result instanceof ResponseEntity) {
            int statusCode = ((ResponseEntity<?>) result).getStatusCodeValue();
            System.out.println("Response Code: " + statusCode);
        }
    }

}