package com.example.backend.controller;

import com.example.backend.dto.StudentDTO;
import com.example.backend.service.StudentService;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Refill;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
@CrossOrigin("*")
public class StudentController {
    @Autowired
    private StudentService studentService;
    private final Bucket bucket;

    public StudentController() {
        Bandwidth limit = Bandwidth.classic(10, Refill.greedy(10, Duration.ofMinutes(1)));
        this.bucket = Bucket.builder()
                .addLimit(limit)
                .build();
        System.out.println(bucket.getAvailableTokens());
    }

    // Build get all student REST API
    @GetMapping
    public ResponseEntity<List<StudentDTO>> getAllStudent() {
        if(bucket.tryConsume(1)) {
            List<StudentDTO> studentDTOList = studentService.getAllStudent();
            return ResponseEntity.ok(studentDTOList);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    // Build get student by ID
    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> getStudentById(@PathVariable Long id) {
        StudentDTO studentDTO = studentService.getStudentById(id);
        return ResponseEntity.ok(studentDTO);
    }

    // Build add student
    @PostMapping
    public ResponseEntity<StudentDTO> createStudent(@RequestBody StudentDTO studentDTO) {
        StudentDTO newStudent = studentService.addStudent(studentDTO);
        return new ResponseEntity<>(newStudent, HttpStatus.CREATED);
    }

    // Build update student
    @PutMapping("/{id}")
    public ResponseEntity<StudentDTO> updateStudent(@PathVariable Long id, @RequestBody StudentDTO studentDTO) {
        StudentDTO updatedStudent = studentService.updateStudent(id, studentDTO);
        return new ResponseEntity<>(updatedStudent, HttpStatus.OK);
    }

    // Build delete student
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable Long id) {
        studentService.deleteStudent(id);
        return new ResponseEntity<>("A student with ID: " + id + " has been deleted" , HttpStatus.ACCEPTED);
    }
}
